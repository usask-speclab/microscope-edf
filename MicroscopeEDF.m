% Determining surface roughness from a series of microscop images.
% For dimensions to be accurate, images should be one minor tic apart on
% the AmScope stage (0.001 mm)
% Images should be labelled sequentially with '-XXXX' as a suffix
% Reisha D Peters, University of Saskatchewan, 2018

clear 
close all

% Input from the user the basefile name and determine how many images
basefilename = input('What is the name of the basefile(ie: sandpaper)? ','s');
numim = length(dir([basefilename,'-*']));

for i = 1:numim % Cycle through the images to import and slice by colour
    CI = imread(strcat(basefilename,'-', sprintf('%04d',i), '.png'));
    ColIR(:,:,i) = (CI(:,:,1)); % Red 
    ColIG(:,:,i) = (CI(:,:,2)); % Blue
    ColIB(:,:,i) = (CI(:,:,3)); % Green
    I(:,:,i) = (rgb2gray(CI));
    % Calculated the gradient for each image
    [mag(:,:,i),dir(:,:,i)] = imgradient(I(:,:,i));
end

% Find the maximum gradient for each pixel in the image series
[immax,asurface] = max(mag,[],3);

% Filter the image and set matrices for reconstructing single images 
% Size of filter can be adjusted for expected surface feature size
asurface = medfilt2(asurface,[10,10],'symmetric');

newimcol = uint8(zeros(size(I,1),size(I,2),3));
newimage = uint8(zeros(size(I,1),size(I,2)));
for i = 1:numim % Reconstruct single images in B&W and colour
    newimcol(:,:,1) = newimcol(:,:,1)+ColIR(:,:,i).*(uint8(round(asurface) == i));
    newimcol(:,:,2) = newimcol(:,:,2)+ColIG(:,:,i).*(uint8(round(asurface) == i));
    newimcol(:,:,3) = newimcol(:,:,3)+ColIB(:,:,i).*(uint8(round(asurface) == i));
    newimage = newimage+I(:,:,i).*(uint8(round(asurface) == i)); % Black and white
end

% Use a gaussian filter to smooth surface curvature 
% Size of filter can be adjusted for expected surface feature size
asurface = imgaussfilt(asurface,15);

% myimheight=asurface;
% Create heigh map of the sample
asurface = asurface*0.001; % Converts to actual surface height
figure(1), imagesc(asurface)
cb = colorbar('south');
colormap('gray');
set(cb,'position',[0.15,0.12,0.5,0.02],'AxisLocation','in');
axis off
text(1700,1700,'mm');

figure(2), imshow(newimage) % B&W EDF
figure(3), imshow(newimcol) % Colour EDF

% Reduce the image size for 3D plot to improve speed
myreduc = 0.3; 
smallnewim = imresize(newimcol,myreduc);
cellheights = imresize(asurface,myreduc);

x = (1:size(cellheights,1));
y = (1:size(cellheights,2));
cellheights = cellheights-0.9*min(min(cellheights)); % Remove extra images

% 3D plots
figure(4), surf(y,x,cellheights,'EdgeColor','none');
figure(5), surf(y,x,cellheights,smallnewim,'FaceColor','texturemap','EdgeColor','none');
zlim([0.0,0.06])
title('Height in mm')

% Same EDF and Height Map images
imwrite(newimcol,strcat('EDF_',basefilename,'.png'));
saveas(figure(1),strcat('HM_',basefilename,'.png'));
